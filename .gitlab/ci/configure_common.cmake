set(CTEST_USE_LAUNCHERS "ON" CACHE STRING "")

set(FIDES_ENABLE_TESTING "ON" CACHE BOOL "")
set(VTKm_DIR "/opt/vtkm/lib/cmake" CACHE STRING "")
set(ADIOS2_DIR "/opt/adios2/" CACHE STRING "")

include("${CMAKE_CURRENT_LIST_DIR}/configure_sccache.cmake")
